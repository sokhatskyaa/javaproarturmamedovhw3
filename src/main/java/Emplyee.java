public class Emplyee {
    private String name;
    private String surname;
    private String patronymic;
    private String position;
    private String email;
    private int numberPhone;
    private int age;

    public Emplyee(String name, String surname, String patronymic, String position, String email, int numberPhone, int age) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.position = position;
        this.email = email;
        this.numberPhone = numberPhone;
        this.age = age;
    }
}
